# -*- coding: utf-8 -*-
# Created by duyhung at 4/16/20 17:50
import time
from web3 import Web3, HTTPProvider

w3 = Web3(HTTPProvider("http://localhost:8545"))


def send_ether_to_contract(amount_in_ether):
    amount_in_wei = w3.toWei(amount_in_ether, 'ether')

    from_wallet_address = "0xF8B76F004eDe3122470234F05b8350478f0415d6"
    from_wallet_private_key = "3B9F65F79DE44B20971B57485D27332748485523702E5D87DEC33D3E24ADC2F1"
    to_wallet_address = "0x9F7Cb012214b2F6ce99bC990cB56C2BB7D31cbEf"
    nonce = w3.eth.getTransactionCount(from_wallet_address)

    txn_dict = {
        'to': to_wallet_address,
        'value': amount_in_wei,
        'gas': 2000000,
        'gasPrice': w3.toWei('40', 'gwei'),
        'nonce': nonce,
        'chainId': 8813
    }

    signed_txn = w3.eth.account.signTransaction(txn_dict, from_wallet_private_key)
    txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)

    txn_receipt = None
    count = 0
    while txn_receipt is None and (count < 30):
        time.sleep(10)
        txn_receipt = w3.eth.getTransactionReceipt(txn_hash)
        print(txn_receipt)

    if txn_receipt is None:
        return {'status': 'failed', 'error': 'timeout'}
    return {'status': 'added', 'txn_receipt': txn_receipt}


if __name__ == '__main__':
    send_ether_to_contract(20)
