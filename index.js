const fs = require('fs');
const Web3 = require('web3');
var Tx = require('ethereumjs-tx').Transaction;


const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

const actionAdd = 1;
const sendTransaction = 2;

const action = sendTransaction; 

function createAccount() {
  var accountSender = web3.eth.accounts.create();
  return accountSender;
}

function sendETH(accountSender_address,accountSender_privateKey,accountReceiver_address,numETH=0) {
  // accountSender_privateKey bỏ 0x khi truyền vào
  
  web3.eth.isMining().then(item => {
    console.log('isMining',item)
  });
  // web3.eth.getCoinbase().then(console.log)
  // return;
 
  var privateKeyBuffer = new Buffer.from(accountSender_privateKey, 'hex');
 
  web3.eth.net.getId().then(chainId => {
    console.log('timestame',new Date().getTime())
    // var rawTx = {
    //   from: accountSender_address,
    //   nonce: web3.utils.toHex(new Date().getTime()),
    //   gasPrice: web3.utils.toHex(parseInt(210000)),
    //   gasLimit: web3.utils.toHex(210000),
    //   to: accountReceiver_address,
    //   value: '0x'+numETH,
    //   data: '0x00'
    // }

    var rawTx = {
      from: accountSender_address,
      nonce: 0x0,
      gasPrice: '0x33450',
      gasLimit: '0x33450',
      to: accountReceiver_address,
      value: '0x1000',
      data: '0x00'
    }

    var tx = new Tx(rawTx, {chain: chainId});
    tx.sign(privateKeyBuffer);
    var serializedTx = tx.serialize();
    console.log('rawTx',rawTx)
    console.log('serializedTx','0x' + serializedTx.toString('hex'))
    return web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex')).on('receipt', console.log);
  });
}
 
// action
if(action==actionAdd) {
 console.log('createAccount()',createAccount())
}


 
 
if(action==sendTransaction) {
  // action
 //account_root
 
// const accountSender_address = '0xc05198239aBeD0CE89C6cf36c4f0758c9093b95B';
 const accountSender_address = '0xF8B76F004eDe3122470234F05b8350478f0415d6';

 //const accountSender_privateKey = '7cf92f85781aa4ef7b6398acf14f8b4ff71cae7b3bbd67e2ea990006dc611c31';
 const accountSender_privateKey = '3B9F65F79DE44B20971B57485D27332748485523702E5D87DEC33D3E24ADC2F1'; 
//const accountReceiver_address = '0x086c039a1540880F30BD298A9bBcaC09334EB524';
const accountReceiver_address = '0x9F7Cb012214b2F6ce99bC990cB56C2BB7D31cbEf';
  console.log('sendETH()',sendETH(accountSender_address,accountSender_privateKey,accountReceiver_address,100));
  return;
}
