//STEP 0: Start server + miner:
// cd to folder contain datadir
// geth --rpc --networkid 8813 --rpcapi="eth,net,personal,web3" --rpccorsdomain "*" --datadir "./datadir" --allow-insecure-unlock console
// >miner.start()

var Web3 = require('web3');
var Personal = require('web3-eth-personal');

// "Personal.providers.givenProvider" will be set if in an Ethereum supported browser.
// var personal = new Personal(Personal.givenProvider || 'http://localhost:8545');

// var web3 = new Web3(new Web3.providers.HttpProvider('http://45.76.150.219:8545'));
ar web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));

var sender_address = "0xF8B76F004eDe3122470234F05b8350478f0415d6"; //Trong server ethereum
var sender_private_key = "3B9F65F79DE44B20971B57485D27332748485523702E5D87DEC33D3E24ADC2F1"
var to_receiver_address = "0x9F7Cb012214b2F6ce99bC990cB56C2BB7D31cbEf";  //Tao ra boi nodejs
var receiver_address1 = "0xa0a4ffA58f503f7bFe00071CeDd5a6A69452F780"; //Trong server ethereum


web3.eth.getCoinbase()
.then(console.log);//0xf8b76f004ede3122470234f05b8350478f0415d6

web3.eth.getProtocolVersion()
.then(console.log); //0x41

web3.eth.isMining()
.then(console.log); //false

web3.eth.getHashrate()
.then(console.log); //0

web3.eth.getGasPrice()
.then(console.log);//1000000000

web3.eth.getAccounts()
.then(console.log); 
// [
//   '0xF8B76F004eDe3122470234F05b8350478f0415d6',
//   '0xa0a4ffA58f503f7bFe00071CeDd5a6A69452F780'
// ]

//Get code
console.log("Code:")
web3.eth.getCode("0xF8B76F004eDe3122470234F05b8350478f0415d6")
.then(console.log);


web3.eth.getBlockNumber()
.then(console.log);//4535

web3.eth.getBalance(sender_address)
.then(console.log);//322173799999996999925930

// web3.eth.getBlock(4535)
// .then(console.log);
// {
//   difficulty: '766370',
  // gasLimit: 8000000,
  // gasUsed: 0,
  // hash: '0x8e51b503dc8b9fd08dfb5cb5b58095c6b4e94c634cbf2cfe261ceceea87c6783',
  // miner: '0xF8B76F004eDe3122470234F05b8350478f0415d6',
  // mixHash: '0x5c448f2e4d1706111735c3f7bc93f101a9864f513dbc94f35e9c77640d1fc80b',
  // nonce: '0x5455d9968159fbd6',
  // number: 4535,
  // ...}

// web3.eth.getBlockTransactionCount("0x8e51b503dc8b9fd08dfb5cb5b58095c6b4e94c634cbf2cfe261ceceea87c6783")
// .then(console.log);//0


web3.eth.getNodeInfo().then(console.log);


function send_ether_to_contract(amount_in_ether) {
  var amount_in_wei = web3.utils.toWei(amount_in_ether, 'ether');
  var from_wallet_address = "0xF8B76F004eDe3122470234F05b8350478f0415d6"
  var from_wallet_private_key = "3B9F65F79DE44B20971B57485D27332748485523702E5D87DEC33D3E24ADC2F1"
  var to_wallet_address = "0x9F7Cb012214b2F6ce99bC990cB56C2BB7D31cbEf"
  var nonce = web3.eth.getTransactionCount(from_wallet_address)
.then(console.log);

}

function sign_callback(abc){
  console.log("Signed")
  console.log(abc)
}

function transaction_callback(abc){
  console.log("Transaction Signed")
  console.log(abc)
}

function unlockcallback(abc){
  console.log("Unlocked")
  // console.log(abc)
}

function send_transaction_callback(abc){
  console.log("Send Transaction Signed")
  console.log(abc)
}
// ======================STEP 1===============================
// Step 1: Unlock account, 0 = unlimitted time, 15000=
// web3.eth.personal.unlockAccount(address, password, unlockDuraction [, callback])
web3.eth.personal.unlockAccount(sender_address, '123456', 15000, unlockcallback)

// ======================STEP 2: SIGN (Optional)===============================
// web3.eth.sign("123456", sender_address, sign_callback)


// ======================STEP 3: GET new Nonce===============================
// web3.eth.signTransaction(transactionObject, address [, callback])
web3.eth.getTransactionCount(sender_address).then(txCount => {
      const newNonce = web3.utils.toHex(txCount)
      var amount_in_wei = web3.utils.toWei('20', 'ether'); //Gui 20 ether
      var gasPrice = web3.utils.toWei('40', 'gwei');

      //==========Doan nay OK nhung cung ko can===========
    //   web3.eth.signTransaction({
    //     nonce: newNonce,
    //     from: sender_address,
    //     gasPrice: gasPrice,
    //     gas: 2000000,
    //     to: to_receiver_address,
    //     value: amount_in_wei,
    //     // data: ""
    //     chainId: 8813
    // }, sender_address, transaction_callback)

    // ======================STEP 4: SEND ETHER===============================
     web3.eth.sendTransaction({
        nonce: newNonce,
        from: sender_address,
        gasPrice: gasPrice,
        gas: 2000000,
        to: to_receiver_address,
        value: amount_in_wei,
        // data: ""
        chainId: 8813

    }, '123456', send_transaction_callback); //password, callback
     //============================OK DONE=================================


  });


