SETUP:
brew tap ethereum/ethereum
brew install ethereum

➜  server pwd
/Users/duyhung/git/ether/server
➜  server which geth
/usr/local/bin/geth

➜ geth -v
...
VERSION:
   1.9.12-stable-b6f1c8dc


Step 0: Tạo thư mục rỗng 
mkdir -p ~git/ether/server 

Step 1: Tạo 2 tài khoản: Dùng để làm tài khoản gửi và nhận 
cd ~/git/ether/server
Xoá datadir nếu đã tồn tại: rm -rf datadir 

geth --datadir datadir account new
pass: 123456

==> 
Public address of the key:   0x3727fA7885aB954e542dCdE489EA71e3E8E99303
Path of the secret key file: datadir/keystore/UTC--2022-04-15T02-30-47.086926000Z--3727fa7885ab954e542dcde489ea71e3e8e99303

geth --datadir datadir account new
pass: 123456

==>
Public address of the key:   0x13F84B3a0653100408C14464Cb7cBffe696a9761
Path of the secret key file: datadir/keystore/UTC--2022-04-15T02-31-36.636197000Z--13f84b3a0653100408c14464cb7cbffe696a9761


Step 2a: Tạo 1 file sender.json 
cd ~/git/ether/server
touch sender.json

Step 2b: Copy địa chỉ ví thứ nhất vào file sender.json (đóng vai trò là genesis block - là block số 0 đầu tiên của mạng blockchain)
CHÚ Ý: Chỉ copy từ sau "0x" thôi
"3727fA7885aB954e542dCdE489EA71e3E8E99303"

sender.json:
{
    "config": {
        "chainId":8813,
        "nonce":0,
        "homesteadBlock": 0,
        "eip155Block": 0,
        "eip158Block": 0,
        "eip150Block": 0
    },
   "difficulty": "0x100",
    "gasLimit": "0x4000000",
    "alloc": {
        "3727fA7885aB954e542dCdE489EA71e3E8E99303": {
        "balance": "300000000000000000000000"
     }
    }
}

Step 3: Khởi tạo số dư cho ví (người gửi)
$geth --datadir ./datadir init sender.json

➜  server geth --datadir ./datadir init sender.json
INFO [04-15|09:34:08.348] Maximum peer count                       ETH=50 LES=0 total=50
INFO [04-15|09:34:08.375] Allocated cache and file handles         database=/Users/duyhung/git/ether/server/datadir/geth/chaindata cache=16.00MiB handles=16
INFO [04-15|09:34:08.442] Writing custom genesis block
INFO [04-15|09:34:08.444] Persisted trie from memory database      nodes=1 size=149.00B time=1.566133ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [04-15|09:34:08.445] Successfully wrote genesis state         database=chaindata hash=2f0e68…368eee
INFO [04-15|09:34:08.445] Allocated cache and file handles         database=/Users/duyhung/git/ether/server/datadir/geth/lightchaindata cache=16.00MiB handles=16
INFO [04-15|09:34:08.489] Writing custom genesis block
INFO [04-15|09:34:08.490] Persisted trie from memory database      nodes=1 size=149.00B time=768.485µs  gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [04-15|09:34:08.491] Successfully wrote genesis state         database=lightchaindata hash=2f0e68…368eee

Step 4: Chạy blockchain 
$geth --rpc --networkid 8813 --rpcapi="eth,net,personal,web3" --rpccorsdomain "*" --datadir "./datadir" --allow-insecure-unlock console

Step 5: Kiểm tra accounts:
> eth.accounts
["0x3727fa7885ab954e542dcde489ea71e3e8e99303", "0x13f84b3a0653100408c14464cb7cbffe696a9761"]

Step 6: Unlock account (trước khi kiểm tra số dư)
>personal.unlockAccount(eth.accounts[0], "123456")
>personal.unlockAccount(eth.accounts[1], "123456")

Step 7: Kiểm tra số dư tk:
> eth.getBalance("0x3727fa7885ab954e542dcde489ea71e3e8e99303")
3e+23

==> OK đã có số dư 

======================CHẠY Miner thì nó sẽ sinh ra thêm coin===============
Step 8: Start miner:
>miner.start()

Step 11: Kiểm tra số dư tk lần nữa:
> eth.getBalance("0x3727fa7885ab954e542dcde489ea71e3e8e99303")
3.003e+23
===> SỐ DƯ ĐÃ TĂNG LÊN 

Chờ một lúc cho nó mining và kiểm tra lại:
> eth.getBalance("0x3727fa7885ab954e542dcde489ea71e3e8e99303")
3.00505e+23

Step 12: Stop miner:
>miner.stop()

Step 13: Stop server: 
> exit 

Step 14: Start server again:
$geth --rpc --networkid 8813 --rpcapi="eth,net,personal,web3" --rpccorsdomain "*" --datadir "./datadir" --allow-insecure-unlock console


=======================
OLD

1. Start Server Ether
   cd ~/git/ether/server
   geth --rpc --networkid 8813 --rpcapi="eth,net,personal,web3" --rpccorsdomain "*" --datadir "./datadir" --allow-insecure-unlock console

   Mở terminal khác:
   Tạo tài khoản mới:
   geth account new

➜ server geth --datadir ./datadir account new
INFO [04-08|11:54:30.760] Maximum peer count                       ETH=50 LES=0 total=50
Your new account is locked with a password. Please give a password. Do not forget this password.
Password:
Repeat password:

Your new key was generated

Public address of the key:   0x2613e7EA9C0F122657bfba1C31b7D3a3584bb8d6
Path of the secret key file: datadir/keystore/UTC--2022-04-08T04-54-38.437695000Z--2613e7ea9c0f122657bfba1c31b7d3a3584bb8d6

- You can share your public address with anyone. Others need it to interact with you.
- You must NEVER share the secret key with anyone! The key controls access to your funds!
- You must BACKUP your key file! Without the key, it's impossible to access account funds!
- You must REMEMBER your password! Without the password, it's impossible to decrypt the key!



- 
const connection_str = 'mongodb+srv://admin:Admin$12345@cluster0-8mqh2.mongodb.net/soccer?retryWrites=true&w=majority'



Tạo private ether network:
1. Download GETH version 1.9.12
 
https://geth.ethereum.org/downloads/
https://gethstore.blob.core.windows.net/builds/geth-darwin-amd64-1.9.12-b6f1c8dc.tar.gz
cd ~/Downloads
tar -zxvf geth-darwin-amd64-1.9.12-b6f1c8dc.tar.gz
sudo mv geth /usr/local/bin
which geth
/usr/local/bin/geth
OK

2. mkdir private-ethereum & cd private-ethereum 
3. Tạo tài khoản ví Eth cho người gửi
   $geth --datadir ./datadir account new
	gõ password 123456
==>

Public address of the key:   0x0fB30901a38A237383EFAcCC318441056e049B89
Path of the secret key file: datadir/keystore/UTC--2022-04-14T17-57-03.094674000Z--0fb30901a38a237383efaccc318441056e049b89


1. Tạo tài khoản ví Eth cho người nhận 
	$geth --datadir ./datadir account new
	gõ password 123456
==>

new

Your new key was generated 2022

Tao vi 1: 

Public address of the key:   0xb6CBc00973eA22286b49A4DFC42DA72b9Cf2d8E7
Path of the secret key file: datadir/keystore/UTC--2022-04-14T18-09-15.764592000Z--b6cbc00973ea22286b49a4dfc42da72b9cf2d8e7

Public address of the key:   0x809Db17B3572548028F10E8b07e6725d94025477
Path of the secret key file: datadir/keystore/UTC--2022-04-14T18-09-49.534522000Z--809db17b3572548028f10e8b07e6725d94025477


1. Copy địa chỉ ví người gửi vào file sender.json như sau
sender.json 
{
    "config": {
        "chainId":8813,
        "nonce":0,
        "homesteadBlock": 0,
        "eip155Block": 0,
        "eip158Block": 0,
        "eip150Block": 0
    },
   "difficulty": "0x100",
    "gasLimit": "0x4000000",
    "alloc": {
        "0xb6CBc00973eA22286b49A4DFC42DA72b9Cf2d8E7": {
           "balance": "300000000000000000000000"
     }
    }
}

2. stop server
3. Xoá thư mục datadir đi 
4. chạy bước 5: =>OK 


5.  Khởi tạo ví (nạp tiền vào ví)
	$geth --datadir ./datadir init ./sender.json

7. Chạy server Ethereum và vào console luôn: 
	$geth --rpc --networkid 8813 --rpcapi="eth,net,personal,web3" --rpccorsdomain "*" --datadir "./datadir" --allow-insecure-unlock console
8. Test thử trên console 
	> eth.accounts
["0xf8b76f004ede3122470234f05b8350478f0415d6", "0xa0a4ffa58f503f7bfe00071cedd5a6a69452f780"]
==> Có 2 tài khoản ví 
9. Unlock account:
>personal.unlockAccount(eth.accounts[0], "123456")
>personal.unlockAccount(eth.accounts[1], "123456")

10. Start miner:
>miner.start()
11. Gửi Eth:
eth.sendTransaction({from:"0xF8B76F004eDe3122470234F05b8350478f0415d6", to:"0xa0a4ffA58f503f7bFe00071CeDd5a6A69452F780", value: web3.toWei(20, "ether")})
INFO [04-14|16:04:41.563] Submitted transaction                    fullhash=0xdbe2285092ba0e4a716dc818f6808c07bcfc9d7d024995712a2f0a44e5f3dae3 recipient=0xa0a4ffA58f503f7bFe00071CeDd5a6A69452F780
"0xdbe2285092ba0e4a716dc818f6808c07bcfc9d7d024995712a2f0a44e5f3dae3"
==> OK DONE!

12. Download MetaMask Chrome add-on: https://metamask.io/download.html
13. Add vào Chrome 
14. Tạo tài khoản MetaMask: Chọn "Create a Wallet"
￼

13. Chọn biểu tượng MetaMask trên góc phải Chrome => Chọn connect network: localhost:8545
￼

14. Import account vào MetaMask: Import private key
Copy 2 private key đã tạo ở trên vào thì nó sẽ import 2 tài khoản đó vào MetaMask

Xem thêm: 
https://www.edureka.co/blog/ethereum-private-network-tutorial
https://medium.com/publicaio/how-import-a-wallet-to-your-metamask-account-dcaba25e558d


node index.js
createAccount() {
  address: '0x9F7Cb012214b2F6ce99bC990cB56C2BB7D31cbEf',
  privateKey: '0x649802a357e60f8c10ecb6ea573295d9529f9c0d3b0f10acc05ac945fba5ad90',
  signTransaction: [Function: signTransaction],
  sign: [Function: sign],
  encrypt: [Function: encrypt]
}

===============
Set up Heroku Nodejs

heroku login
heroku create footballbet

https://footballbet.herokuapp.com/ | https://git.heroku.com/footballbet.git

cd git/nodejs
git clone https://github.com/heroku/node-js-getting-started.git footballbet
cd footballbet

➜  footballbet git:(master) git remote add heroku git@heroku.com:footballbet.git



===========
28/4/2020
Tạo Server Private Ethereum trên vultr
Ubuntu 19.04
45.76.150.219
ssh -i ~/.ssh/postviet root@45.76.150.219


sudo apt-get install software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install ethereum


mkdir private_ethereum
cd private_ethereum
1. Tạo ví cho Sender:
geth --datadir ./datadir account new

password: 123456
Your new key was generated
Public address of the key:   0x5D93A3B9DCBa56cd834F5fEb07Cad7278A267fCF
Path of the secret key file: datadir/keystore/UTC--2020-04-28T15-02-56.147293202Z--5d93a3b9dcba56cd834f5feb07cad7278a267fcf


2. Tạo ví người nhận
geth --datadir ./datadir account new
pass: 123456

Your new key was generated
Public address of the key:   0xe50C7422441Fafe6E89c69D55212a6b29D9387a1
Path of the secret key file: datadir/keystore/UTC--2020-04-28T15-03-25.640508463Z--e50c7422441fafe6e89c69d55212a6b29d9387a1

3. Ví cho user:
Your new key was generated
Public address of the key:   0x6D9E8db6f37B29A975c5D17F9999744dc0C22594
Path of the secret key file: datadir/keystore/UTC--2020-04-28T15-03-45.868194896Z--6d9e8db6f37b29a975c5d17f9999744dc0c22594




1. Khởi tạo tiền cho Sender

geth --datadir ./datadir init ./sender.json

4. Test thử 
geth --rpc --networkid 8813 --rpcapi="eth,net,personal,web3" --rpccorsdomain "*" --datadir "./datadir" --allow-insecure-unlock console

==> Có 2 tài khoản ví9. Unlock account:>personal.unlockAccount(eth.accounts[0], "123456")>personal.unlockAccount(eth.accounts[1], "123456")
> eth.getBalance(eth.accounts[0])
1e+28

10. Start miner:>miner.start()11. Gửi Eth:

eth.sendTransaction({from:eth.accounts[0], to:eth.accounts[2], value: web3.toWei(2000, "ether")})


INFO [04-14|16:04:41.563] Submitted transaction                    fullhash=0xdbe2285092ba0e4a716dc818f6808c07bcfc9d7d024995712a2f0a44e5f3dae3 recipient=0xa0a4ffA58f503f7bFe00071CeDd5a6A69452F780"0xdbe2285092ba0e4a716dc818f6808c07bcfc9d7d024995712a2f0a44e5f3dae3"==> OK DONE!


geth --datadir ./datadir --networkid 1114 console 2>> Eth.log





geth --rpc --networkid 8813 --rpcapi="eth,net,personal,web3" --rpcaddr "0.0.0.0" --rpccorsdomain "*" --datadir "./datadir" --allow-insecure-unlock console

 


pm2 start "npm start"

===TAO LAI 

our new key was generated

Public address of the key:   0x5D93A3B9DCBa56cd834F5fEb07Cad7278A267fCF
Path of the secret key file: datadir/keystore/UTC--2020-04-28T15-02-56.147293202Z--5d93a3b9dcba56cd834f5feb07cad7278a267fcf

Your new key was generated

Public address of the key:   0xe50C7422441Fafe6E89c69D55212a6b29D9387a1
Path of the secret key file: datadir/keystore/UTC--2020-04-28T15-03-25.640508463Z--e50c7422441fafe6e89c69d55212a6b29d9387a1

- You can share your public address with anyone. Others need it to interact with you.

Your new key was generated

Public address of the key:   0x6D9E8db6f37B29A975c5D17F9999744dc0C22594
Path of the secret key file: datadir/keystore/UTC--2020-04-28T15-03-45.868194896Z--6d9e8db6f37b29a975c5d17f9999744dc0c22594

- You can share your public address with anyone. Others need it to interact with you.

sender.json
{
    "config": {
        "chainId":8813,
        "nonce":0,
        "homesteadBlock": 0,
        "eip155Block": 0,
        "eip158Block": 0,
        "eip150Block": 0
    },
   "difficulty": "0x100",
    "gasLimit": "0x4000000",
    "alloc": {
        "5D93A3B9DCBa56cd834F5fEb07Cad7278A267fCF": {
           "balance": "10000000000000000000000000000"
     }
    }
}
~

geth --datadir ./datadir init ./sender.json

eth.sendTransaction({from:eth.accounts[0], to:eth.accounts[2], value: web3.toWei(2000, "ether")})

web3.fromWei(eth.getBalance(eth.accounts[0]), "ether")
9999998115

web3.fromWei(eth.getBalance("0x6D9E8db6f37B29A975c5D17F9999744dc0C22594), "ether")
2000

root@ether:~/private_ethereum# geth --rpc --networkid 8813 --rpcapi="eth,net,personal,web3" --rpcaddr "0.0.0.0" --rpccorsdomain "*" --datadir "./datadir" --allow-insecure-unlock console

personal.unlockAccount("0x5D93A3B9DCBa56cd834F5fEb07Cad7278A267fCF", "123456")
personal.unlockAccount("0xe50C7422441Fafe6E89c69D55212a6b29D9387a1", "123456")
personal.unlockAccount("0x6D9E8db6f37B29A975c5D17F9999744dc0C22594", "123456")

web3.fromWei(eth.getBalance("0x5D93A3B9DCBa56cd834F5fEb07Cad7278A267fCF"), "ether") ==> 9999998395
web3.fromWei(eth.getBalance("0xe50C7422441Fafe6E89c69D55212a6b29D9387a1"), "ether") ==> 0
web3.fromWei(eth.getBalance("0x6D9E8db6f37B29A975c5D17F9999744dc0C22594"), "ether") ==> 2000

nohup geth --rpc --networkid 8813 --rpcapi="eth,net,personal,web3" --rpcaddr "0.0.0.0" --rpccorsdomain "*" --datadir ./datadir --allow-insecure-unlock > run.log 2>&1 &



API  NEWS  key from https://newsapi.org/register/success
96d59aece8434292a80528263c2b74da


